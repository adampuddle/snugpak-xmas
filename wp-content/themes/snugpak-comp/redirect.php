<?php
function redirect_user() {
  if ( ! is_user_logged_in() && ! is_page() ) {
    $return_url = esc_url( home_url() );
    wp_redirect( $return_url );
    exit;
  }
}
add_action( 'template_redirect', 'redirect_user' );
?>
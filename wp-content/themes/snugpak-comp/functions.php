<?php

function create_account(){
    //You may need some data validation here
    $user = ( isset($_POST['uname']) ? $_POST['uname'] : '' );
    $pass = ( isset($_POST['upass']) ? $_POST['upass'] : '' );
    $email = ( isset($_POST['uemail']) ? $_POST['uemail'] : '' );

    if ( !username_exists( $user )  && !email_exists( $email ) ) {
       $user_id = wp_create_user( $user, $pass, $email );

       if( !is_wp_error($user_id) ) {
           //user has been created
           $user = new WP_User( $user_id );
           $user->set_role( 'subscriber' );
           //Redirect
           wp_redirect( 'http://localhost/snugpak-competition/thanks' );
           exit;
       } else {
           //$user_id is a WP_Error object. Manage the error
       }
    }

}
add_action('init','create_account');

register_nav_menus( array(
	'main_menu' => 'Main menu',
	'footer_menu' => 'Footer menu',
) );

add_theme_support( 'post-thumbnails' ); 

/*add_action( 'wp_enqueue_scripts', 'theme_scripts_styles' );

/**
 * Proper way to enqueue scripts and styles
 */
/*function laycocks_scripts() {
	wp_enqueue_script('jquery', '/wp-includes/js/jquery/jquery.js', array('jquery')); 
	wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/script.js', array(), '1.0.0', true );
  wp_enqueue_script( 'scripts', get_template_directory_uri() . '/js/jquery-val-min.js', array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'theme_scripts_styles' );*/

add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );


function restrict_admin()
{
  if ( ! current_user_can( 'manage_options' ) && '/wp-admin/admin-ajax.php' != $_SERVER['PHP_SELF'] ) {
                wp_redirect( site_url() );
  }
}


add_action( 'admin_init', 'restrict_admin', 1 );

add_action( 'wp_login_failed', 'pu_login_failed' ); // hook failed login

function pu_login_failed( $user ) {
    // check what page the login attempt is coming from
    $referrer = $_SERVER['HTTP_REFERER'];

    // check that were not on the default login page
  if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') && $user!=null ) {
    // make sure we don't already have a failed login attempt
    if ( !strstr($referrer, '?login=failed' )) {
      // Redirect to the login page and append a querystring of login failed
        wp_redirect( $referrer . '?login=failed');
      } else {
          wp_redirect( $referrer );
      }

      exit;
  }
}

add_action( 'authenticate', 'pu_blank_login');

function pu_blank_login( $user ){
    // check what page the login attempt is coming from
    $referrer = $_SERVER['HTTP_REFERER'];

    $error = false;

    if($_POST['log'] == '' || $_POST['pwd'] == '')
    {
      $error = true;
    }

    // check that were not on the default login page
    if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') && $error ) {

      // make sure we don't already have a failed login attempt
      if ( !strstr($referrer, '?login=failed') ) {
        // Redirect to the login page and append a querystring of login failed
          wp_redirect( $referrer . '?login=failed' );
        } else {
          wp_redirect( $referrer );
        }

    exit;

    }
}

?>
<?php
	if( !is_user_logged_in() ) {
		wp_redirect('http://localhost/snugpak-competition/signin/');
		exit;
	}
?>
<?php get_header(); ?>
<div class="row register">
		<div class="large-3 medium-3 small-12 columns">
			<div class="share">
				<div class="text">
					Share on<br> social media<br> to instantly<br> win mini<br> prizes
				</div>
			</div>
		</div>
		<div class="large-6 medium-6 small-12 columns register">
			<div id="container">
			<?php
if ( is_user_logged_in() ) {
    echo '<a href="'. wp_logout_url() .'">Logout</a>';
}
?>
				<?php $current_user = wp_get_current_user();
					the_field('profile_text','user_'.$current_user->ID)
				?>
			</div>
		</div>
		<div class="large-3 medium-3 small-12 columns how-to-play">
			<div class="opening-text">
				How do <br>I play?
			</div>
			<div class="instructions">
				<p>1) Register your details.</p>

				<p>2) Log in from the 1st December to access Snugpak and find the puzzle pieces.</p>

				<p>3) Share on your Facebook and Twitter that you have found the missing pieces!</p>

				<p>4) Continue everyday and find the other missing puzzle pieces right up until the 12th December.</p>
			</div>
			<div class="end-content">
			<p>Remember you have to play for 12 days in a row to be in with a chance of winning the Snugpak super prize worth £1000!</p>
			<p>Instantly win mini prizes throughout each day you play!</p>
			</div>
			<div class="opening-text">
				Good Luck!
			</div>
		</div>
	</div>
<?php get_footer(); ?>
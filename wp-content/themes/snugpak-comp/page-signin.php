<?php get_header() ?>
	<div class="row register">
		<div class="large-6 medium-6 small-12 large-offset-3 medium-offset-3 columns register">
			<div id="container">
				<div class="details-ribbon">Enter your details to register</div>


	<?php $args = array( 'redirect' => site_url() );

	if(isset($_GET['login']) && $_GET['login'] == 'failed')
	{
		?>
			<div id="login-error" style="background-color: #FFEBE8;border:1px solid #C00;padding:5px;">
				<p>Login failed: You have entered an incorrect Username or password, please try again.</p>
			</div>
		<?php
	}

	 $args = array(
	        'echo' => true,
	        'redirect' => 'http://wpsnipp.com',
	        'form_id' => 'loginform',
	        'label_username' => __( '' ),
	        'label_password' => __( '' ),
	        'label_remember' => __( 'Remember Me' ),
	        'label_log_in' => __( 'Log In' ),
	        'id_username' => 'user_login',
	        'id_password' => 'user_pass',
	        'id_remember' => 'rememberme',
	        'id_submit' => 'wp-submit',
	        'remember' => false,
	        'value_username' => NULL,
	        'value_remember' => false );

	wp_login_form( $args );
	?>
			</div>
		</div>
		<div class="large-3 medium-3 small-12 columns how-to-play">
			<div class="opening-text">
				How do <br>I play?
			</div>
			<div class="instructions">
				<p>1) Register your details.</p>

				<p>2) Log in from the 1st December to access Snugpak and find the puzzle pieces.</p>

				<p>3) Share on your Facebook and Twitter that you have found the missing pieces!</p>

				<p>4) Continue everyday and find the other missing puzzle pieces right up until the 12th December.</p>
			</div>
			<div class="end-content">
			<p>Remember you have to play for 12 days in a row to be in with a chance of winning the Snugpak super prize worth £1000!</p>
			<p>Instantly win mini prizes throughout each day you play!</p>
			</div>
			<div class="opening-text">
				Good Luck!
			</div>
		</div>
	</div>

<script>
jQuery(document).ready(function(){
    jQuery('#user_login').attr('placeholder', 'Name');
    jQuery('#user_email').attr('placeholder', 'Email');
    jQuery('#user_pass').attr('placeholder', 'Password');
});
</script>
<?php get_footer() ?>
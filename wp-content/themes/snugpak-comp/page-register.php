<?php get_header() ?>

	<div class="row register">
	<div class="large-6 medium-6 small-12 large-offset-3 medium-offset-3 columns register">
		<div id="container">
			<div class="details-ribbon">Enter your details to register</div>
			<form id="register-form" method="post" name="myForm">
				<?php $name = htmlspecialchars($_POST['uname']); ?>
				<input placeholder="Name" type="text"  name="uname" value="<?php if (isset($name)) { echo $name; }?>"/>
				<?php $name = htmlspecialchars($_POST['uemail']); ?>
				<input placeholder="Email Address" id="email" type="text" name="uemail" value="<?php if (isset($name)) { echo $name; }?>"/>
				<input placeholder="Password" type="password"  name="upass"/>
				<input class="button" type="submit" value="Submit" />
			</form>

			<?php 
			if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    			$username = $_POST['uname'];
					if($username == "") {?>
					<div class="error-mess">
						You have left the username field blank
					</div>
				<?php }

				$email = $_POST['uemail'];
					if($email == "") { ?>
					<div class="error-mess">
						You have left the email field blank
					</div>
				<?php }
				$email = $_POST['uemail'];
					if ( email_exists( $email ) ) ?>
					<div class="error-mess">
						This email address has already been used. Please use another or click here to <a href="<?php bloginfo('url') ?>/signin/">login</a>/ request a new password
					</div>
					<?php 
					 if (!filter_var($email, FILTER_VALIDATE_EMAIL)) { ?>
					 <div class="error-mess">
					 	You have entered an invalid email address. Please check and re-enter your email address
					 </div>
					 <?php }

				$password = $_POST['upass'];
					if($password == "") { ?>
					<div class="error-mess">
						You have left the password field blank
					</div>
				<?php }
    				 
    		}
			?>
		</div>
	</div>
	<div class="large-3 medium-3 small-12 columns how-to-play">
		<div class="opening-text">
			How do <br>I play?
		</div>
		<div class="instructions">
			<p>1) Register your details.</p>

			<p>2) Log in from the 1st December to access Snugpak and find the puzzle pieces.</p>

			<p>3) Share on your Facebook and Twitter that you have found the missing pieces!</p>

			<p>4) Continue everyday and find the other missing puzzle pieces right up until the 12th December.</p>
		</div>
		<div class="end-content">
		<p>Remember you have to play for 12 days in a row to be in with a chance of winning the Snugpak super prize worth £1000!</p>
		<p>Instantly win mini prizes throughout each day you play!</p>
		</div>
		<div class="opening-text">
			Good Luck!
		</div>
	</div>

</div>
	
<?php get_footer() ?>
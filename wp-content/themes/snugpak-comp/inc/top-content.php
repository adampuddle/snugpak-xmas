<?php
if (is_front_page()) {?>
<ul class="bxslider">
  <?php

// check if the repeater field has rows of data
if( have_rows('slider') ):

  // loop through the rows of data
    while ( have_rows('slider') ) : the_row(); ?>
  <li style="background-image: url('<?php the_sub_field('background_image'); ?>');" />
      <div class="row">
        <div class="large-12 medium-10 small-12 slider-container columns medium-centered">
          <div class="one slider-text animated fadeInDown">
            <?php the_sub_field('slider_text') ?>
          </div>
          <a href="<?php the_sub_field('slider_link') ?>">
            <div class="find-out-more animated fadeInUp">
              Find out more
            </div>
          </a>
        </div>
      </div>
  </li>
  <?php endwhile; endif; ?>
</ul>

<?php } else {
  if ( have_posts() ) : while ( have_posts() ) : the_post(); 
  ?>
  <div class="header-image">
    <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); }?>
  </div>
  <?php endwhile; endif; 
} ?>